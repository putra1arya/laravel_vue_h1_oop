// <?php

// trait Hewan{
//     public $nama;
//     public $darah = 50;
//     public $jumlah_kaki;
//     public $keahlian;

//     public function atraksi(){
//         return $this->nama . " sedang " . $this->keahlian;
//     }
// }
// trait Fight{
//     public $attackPower;
//     public $defencePower;

//     public function serang($penyerang,$korban){
//         return $penyerang . " sedang menyerang " . $korban;
//     }

//     public function diserang($korban,$darah_korban, $atpowerpenyerang, $defpowerkorban){
//         $this->darah = $darah_korban - ($atpowerpenyerang/$defpowerkorban);
//         return $korban . " sedang diserang";
//     }
// }

// class Harimau{
//     use Hewan,Fight;

//     public function getInfoHewan(){
//         return 'Jenis Hewan = ' . $this->nama . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'darah = ' . $this->darah . '<br>' . 'keahlian = ' .$this->keahlian . '<br>' . 'attack Power = ' . $this->attackPower . '<br>' . "defence Power = " . $this->defencePower;  
//     }
// }

// class Elang{
//     use Hewan,Fight;

//     public function getInfoHewan(){
//         return 'Jenis Hewan = ' . $this->nama . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'darah = ' . $this->darah . '<br>' . 'keahlian = ' .$this->keahlian . '<br>' . 'attack Power = ' . $this->attackPower . '<br>' . "defence Power = " . $this->defencePower;  
//     }
// }

// $elang = new Elang();
// $elang->nama = "Elang";
// $elang->jumlah_kaki = 2;
// $elang->keahlian = "terbang tinggi";
// $elang->attackPower = 10;
// $elang->defencePower = 5;

// $harimau = new Harimau();
// $harimau->nama = "Harimau";
// $harimau->jumlah_kaki = 4;
// $harimau->keahlian = "lari cepat";
// $harimau->attackPower = 7;
// $harimau->defencePower = 8;

// echo $elang->getInfoHewan(). '<br><br>';
// echo $harimau->getInfoHewan(). '<br><br>';
// echo $harimau->diserang($harimau->nama, $harimau->darah,$elang->attackPower,$harimau->defencePower ) . '<br> <br>' ;
// echo "Darah Harimau sekarang = ".$harimau->darah . '<br><br>';
// echo "Darah Elang Sekarang = " .$elang->darah.'<br><br>';
// echo $harimau->serang($harimau->nama,$elang->nama) .'<br><br>';
// echo $elang->diserang($elang->nama,$elang->darah,$harimau->attackPower,$elang->defencePower).'<br><br>';
// echo "Darah Harimau sekarang = ".$harimau->darah . '<br><br>';
// echo "Darah Elang Sekarang = " .$elang->darah.'<br><br>';
// ?>
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlah_kaki;
    public $keahlian;

    public function atraksi(){
        return $this->nama . " sedang " . $this->keahlian;
    }
}
trait Fight{
    public $attackPower;
    public $defencePower;

    public function serang($penyerang,$korban){
        echo $penyerang->nama . " sedang menyerang " . $korban->nama;
        
        echo $this->diserang($korban->nama,$korban->darah,$penyerang->attackPower,$korban->defencePower);
    }

    public function diserang($korban,$darah_korban, $atpowerpenyerang, $defpowerkorban){
        $this->darah = $darah_korban - ($atpowerpenyerang/$defpowerkorban);
        return $korban . " sedang diserang";
    }
}

class Harimau{
    use Hewan,Fight;

    public function getInfoHewan(){
        return 'Jenis Hewan = ' . $this->nama . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'darah = ' . $this->darah . '<br>' . 'keahlian = ' .$this->keahlian . '<br>' . 'attack Power = ' . $this->attackPower . '<br>' . "defence Power = " . $this->defencePower;  
    }
}

class Elang{
    use Hewan,Fight;

    public function getInfoHewan(){
        return 'Jenis Hewan = ' . $this->nama . '<br>' . 'Jumlah Kaki = '. $this->jumlah_kaki . '<br>' . 'darah = ' . $this->darah . '<br>' . 'keahlian = ' .$this->keahlian . '<br>' . 'attack Power = ' . $this->attackPower . '<br>' . "defence Power = " . $this->defencePower;  
    }
}

$elang = new Elang();
$elang->nama = "Elang";
$elang->jumlah_kaki = 2;
$elang->keahlian = "terbang tinggi";
$elang->attackPower = 10;
$elang->defencePower = 5;

$harimau = new Harimau();
$harimau->nama = "Harimau";
$harimau->jumlah_kaki = 4;
$harimau->keahlian = "lari cepat";
$harimau->attackPower = 7;
$harimau->defencePower = 8;

echo $elang->getInfoHewan(). '<br><br>';
echo $harimau->getInfoHewan(). '<br><br>';

$harimau->serang($harimau,$elang) .'<br><br>';
echo "Darah Harimau sekarang = ".$harimau->darah . '<br><br>';
echo "Darah Elang Sekarang = " .$elang->darah.'<br><br>';
?>
