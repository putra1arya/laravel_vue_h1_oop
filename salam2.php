
<?php 
//collision jika 2 trait menggunakan nama fungsi yang sama. ada 2 cara untuk menghindarinya
//cara 1 menyingkirkan method yang tidak dibutuhkan dengan menggunakan fungsi instead of
trait selamat{
    public function salam(){
        return "selamat Pagi<br>";
    }
}

trait sapa{
    public function salam(){
        return "Apa Kabar?";
    }
}

class pesan {
    use selamat, sapa{
        selamat::salam insteadof sapa;
    }
}

$pesan = new pesan();
echo $pesan->salam();
//maka akan keluar selamat pagi karena mengacu pada cara 1.

//cara 2. tetap membawa method yang sama hanya saja diberikan alias dengan menggunakan fungsi as

trait ucapan{
    public function salam1(){
        return "Saya ucapkan selamat pagi untuk kita semua";
    }
}
trait congrat{
    public function salam1(){
        return "Congratulation People";
    }
}

class pesan1 {
    use ucapan, congrat{
        congrat::salam1 insteadof ucapan;
        ucapan::salam1 as salamKedua;
    }
}

$pesan1 = new pesan1();
echo $pesan1->salamKedua();
?>