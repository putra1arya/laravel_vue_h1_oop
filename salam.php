<?php

//trait
// trait Apakabar{
//     public function apaKabar(){
//         return "Apa Kabar?"."</br>";
//     }
// }

// trait selamatPagi{
//     public function selamatPagi(){
//         return "Selamat Pagi";
//     }
// }

// trait salam{
//     use Apakabar,selamatPagi;

// }

// class pesan{
//     use salam;
// }

// $obj = new pesan();
// echo $obj->apaKabar();
// echo $obj->selamatPagi();

//urutan prioritas method dengan trait
// 1. Method turunan akan ditimpa oleh method yang berasal dari trait

class apaKabar{
    //ini akan diturunkan 
    public function apaKabar(){
        return 'Apa Kabar?' ."<br>";
    }
}

trait selamatPagi{

    public function selamatPagi(){
        return 'Selamat Pagi';
    }
    public function apaKabar(){
        return 'Apa kabar kawan?'."<br>";
    }
}

class pesan extends apaKabar {
    use selamatPagi;
}

$obj = new pesan();
echo $obj->apaKabar();
// maka akan ditampilkan Apa kabar kawan? karena mengacu pada aturan 1

//2. Current class method akan menimpa method yang berasal dari trait

trait akuBaik{
    public function akuBaik(){
        return "aku baik saja";
    }
}

class sehat{
    use akuBaik;
    public function akuBaik(){
        return "aku sehat sekali";
    }
}
$sehat = new sehat();
echo $sehat->akuBaik();
//maka akan ditampilkan aku sehat sekali karena mengacu pada aturan 2.
?>